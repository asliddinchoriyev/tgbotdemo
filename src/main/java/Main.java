import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Main extends TelegramLongPollingBot {

    private String helper = null;
    private boolean isCon = false;

    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi api = new TelegramBotsApi();
        try {
            api.registerBot(new Main());
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }

    public static Double toDouble(String str) {
        return Double.parseDouble(str);
    }

    public static String converter(String inMoney, String convertType) {
        Gson gson = new Gson();
        String ans = "";
        try {
            URL url = new URL("https://cbu.uz/uz/arkhiv-kursov-valyut/json/");
            URLConnection urlConnection = url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            Type type = new TypeToken<ArrayList<Currency>>() {}.getType();

            ArrayList<Currency> currencies = gson.fromJson(reader, type);

            String usdRate = "";
            String eurRate = "";
            String cnyRate = "";

            for (Currency currency : currencies) {
                if (currency.getCcy().equals("USD")) {
                    usdRate = currency.getRate();
                }

                if (currency.getCcy().equals("EUR")) {
                    eurRate = currency.getRate();
                }

                if (currency.getCcy().equals("CNY")) {
                    cnyRate = currency.getRate();
                }
            }

            DecimalFormat dec = new DecimalFormat("#.##");


            if (convertType.equals("EURO - USD")) {
                ans =  String.valueOf(dec.format((toDouble(inMoney) * toDouble(eurRate) / toDouble(usdRate))));
            }

            if (convertType.equals("USD - EURO")) {
                ans = String.valueOf(dec.format((toDouble(inMoney) * toDouble(usdRate) / toDouble(eurRate))));
            }

            if (convertType.equals("USD - JUAN")) {
                ans = String.valueOf(dec.format((toDouble(inMoney) * toDouble(usdRate) / toDouble(cnyRate))));
            }

            if (convertType.equals("JUAN - USD")) {
                ans = String.valueOf(dec.format((toDouble(inMoney) * toDouble(cnyRate) / toDouble(usdRate))));
            }

            if (convertType.equals("EURO - JUAN")) {
                ans = String.valueOf(dec.format((toDouble(inMoney) * toDouble(eurRate) / toDouble(cnyRate))));
            }

            if (convertType.equals("JUAN - EURO")) {
                ans = String.valueOf(dec.format((toDouble(inMoney) * toDouble(cnyRate) / toDouble(eurRate)))) ;
            }


        } catch (Exception e) {
            System.out.println("connection failed");
            e.printStackTrace();
        }

        return ans;
    }

    @Override
    public String getBotUsername() {
        return "currency_rate_zeroTohero_bot";
    }

    @Override
    public String getBotToken() {
        return "1793853673:AAEpw7L4uYC0wl_AeduFk876oq6LLHVub8w";
    }

    @Override
    public void onUpdateReceived(Update update) {

        SendMessage sendMessage = new SendMessage();
        String inputText = update.getMessage().getText();
        long chatId = update.getMessage().getChatId();
        sendMessage.setChatId(chatId);


        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row1 = new KeyboardRow();

        KeyboardButton button1 = new KeyboardButton("USD - EURO");
        KeyboardButton button2 = new KeyboardButton("EURO - USD");

        row1.add(button1);
        row1.add(button2);

        KeyboardRow row2 = new KeyboardRow();

        KeyboardButton button3 = new KeyboardButton("USD - JUAN");
        KeyboardButton button4 = new KeyboardButton("JUAN - USD");

        row2.add(button3);
        row2.add(button4);


        KeyboardRow row3 = new KeyboardRow();

        KeyboardButton button5 = new KeyboardButton("EURO - JUAN");
        KeyboardButton button6 = new KeyboardButton("JUAN - EURO");

        row3.add(button5);
        row3.add(button6);

        keyboard.add(row1);
        keyboard.add(row2);
        keyboard.add(row3);

        replyKeyboardMarkup.setKeyboard(keyboard);


        switch (inputText) {
            case "/start":
                sendMessage.setText("Choose convert category : ");
                break;
            case "USD - EURO":
                sendMessage.setText("Enter amount of money :");
                isCon = true;
                helper = "USD - EURO";
                isCon = false;
                break;
            case "EURO - USD":
                sendMessage.setText("Enter amount of money :");
                helper = "EURO - USD";
                isCon = false;
                break;
            case "USD - JUAN":
                sendMessage.setText("Enter amount of money :");
                helper = "USD - JUAN";
                isCon = false;
                break;
            case "JUAN - USD":
                sendMessage.setText("Enter amount of money :");
                helper = "JUAN - USD";
                isCon = false;
                break;
            case "EURO - JUAN":
                sendMessage.setText("Enter amount of money :");
                helper = "EURO - JUAN";
                isCon = false;
                break;
            case "JUAN - EURO":
                sendMessage.setText("Enter amount of money :");
                helper = "JUAN - EURO";
                isCon = false;
                break;
        }

        if (helper != null) {
            if (isCon) {
                String inMoney = update.getMessage().getText();
                String converter = converter(inMoney, helper);
                sendMessage.setText(converter);
            }
            isCon = true;
        }





        try {
            execute(sendMessage);
        } catch (
                TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
