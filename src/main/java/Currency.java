import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Currency {

    private Integer id;
    private String Code;
    @JsonProperty("Ccy")
    private String Ccy;
    @JsonProperty("")
    private String CcyNm_RU;
    @JsonProperty("CcyNm_UZ")
    private String CcyNm_UZ;
    private String CcyNm_UZC;
    private String CcyNm_EN;
    private String Nominal;
    @JsonProperty("Rate")
    private String Rate;
    private String Diff;
    private String Date;

}


